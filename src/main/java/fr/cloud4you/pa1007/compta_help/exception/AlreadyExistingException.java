package fr.cloud4you.pa1007.compta_help.exception;

public class AlreadyExistingException extends Exception {

    public AlreadyExistingException() {
        super("The name you have taken is already taken please change");
    }

    public AlreadyExistingException(String message) {
        super("The name you have taken (" + message + ") is already taken please change");
    }

    public AlreadyExistingException(Throwable cause) {
        super(cause);
    }
}