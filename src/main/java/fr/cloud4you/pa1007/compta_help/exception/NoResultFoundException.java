package fr.cloud4you.pa1007.compta_help.exception;

import javax.annotation.Nullable;

public class NoResultFoundException extends Exception {

    public NoResultFoundException(Boolean conceptionError, @Nullable String message) {
        super((conceptionError)
                      ? "This error is thrown if this error comes from the architecture of the database"
                      : message);
    }

    public NoResultFoundException(Throwable cause) {
        super("Other source of error", cause);
    }
}