package fr.cloud4you.pa1007.compta_help.builder;

import fr.cloud4you.pa1007.compta_help.creators.Income;
import fr.cloud4you.pa1007.compta_help.types.MoneyType;

public class IncomeBuilder {

    private double    amount;
    private String    description;
    private String    name;
    private int       year;
    private int       week;
    private int       month;
    private int       day;
    private MoneyType moneyType;
    private String    id;
    private String    accountName;

    public IncomeBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public IncomeBuilder setAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public IncomeBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public IncomeBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public IncomeBuilder setYear(int year) {
        this.year = year;
        return this;
    }

    public IncomeBuilder setWeek(int week) {
        this.week = week;
        return this;
    }

    public IncomeBuilder setMonth(int month) {
        this.month = month;
        return this;
    }

    public IncomeBuilder setDay(int day) {
        this.day = day;
        return this;
    }

    public IncomeBuilder setMoneyType(MoneyType moneyType) {
        this.moneyType = moneyType;
        return this;
    }

    public IncomeBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public Income createIncome() {
        if (accountName == null){
            return new Income(amount, description, name, year, week, month, day, moneyType, id);
        }else {
            return new Income(amount, description, name, year, week, month, day, moneyType, id, accountName);
        }
    }
}