package fr.cloud4you.pa1007.compta_help.builder;

import fr.cloud4you.pa1007.compta_help.creators.Transaction;
import java.util.Date;

public class TransactionBuilder {

    private String  accountName;
    private double  cashBefore;
    private double  amount;
    private Date    transactionDate;
    private double  accountMoneyBefore;
    private boolean cashToAccount;

    public TransactionBuilder setAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public TransactionBuilder setCashBefore(double cashBefore) {
        this.cashBefore = cashBefore;
        return this;
    }

    public TransactionBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public TransactionBuilder setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public TransactionBuilder setAccountMoneyBefore(double accountMoneyBefore) {
        this.accountMoneyBefore = accountMoneyBefore;
        return this;
    }

    public TransactionBuilder setCashToAccount(boolean cashToAccount) {
        this.cashToAccount = cashToAccount;
        return this;
    }

    public Transaction createTransaction() {
        return new Transaction(accountName, cashBefore, amount, transactionDate, accountMoneyBefore, cashToAccount);
    }
}