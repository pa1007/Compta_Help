package fr.cloud4you.pa1007.compta_help.builder;

import fr.cloud4you.pa1007.compta_help.creators.Account;
import java.util.Date;

public class AccountBuilder {

    private Date   creationDate;
    private double amount;
    private String accountName;
    private String id;
    private String ownerName;

    public AccountBuilder setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public AccountBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public AccountBuilder setAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public AccountBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public AccountBuilder setOwnerName(String ownerName) {
        this.ownerName = ownerName;
        return this;
    }

    public Account createAccount() {
        return new Account(creationDate, amount, accountName, id, ownerName);
    }
}