package fr.cloud4you.pa1007.compta_help.builder;

import fr.cloud4you.pa1007.compta_help.creators.Cost;
import fr.cloud4you.pa1007.compta_help.types.MoneyType;
import fr.cloud4you.pa1007.compta_help.types.UsageType;

public class CostBuilder {

    private double    amount;
    private String    description;
    private String    name;
    private int       year;
    private int       week;
    private int       month;
    private int       day;
    private MoneyType moneyType;
    private String    id;
    private UsageType type;
    private String    bankName;

    public CostBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public CostBuilder setBankName(String bankName) {
        this.bankName = bankName;
        return this;
    }

    public CostBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public CostBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CostBuilder setYear(int year) {
        this.year = year;
        return this;
    }

    public CostBuilder setWeek(int week) {
        this.week = week;
        return this;
    }

    public CostBuilder setMonth(int month) {
        this.month = month;
        return this;
    }

    public CostBuilder setUsageType(UsageType type) {
        this.type = type;
        return this;
    }

    public CostBuilder setDay(int day) {
        this.day = day;
        return this;
    }

    public CostBuilder setMoneyType(MoneyType moneyType) {
        this.moneyType = moneyType;
        return this;
    }

    public CostBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public Cost createCost() {
        if (bankName == null) {
            return new Cost(amount, description, name, year, week, month, day, moneyType, id, type);
        }
        else {
            return new Cost(amount, description, name, year, week, month, moneyType, day, id, type, bankName);
        }
    }
}