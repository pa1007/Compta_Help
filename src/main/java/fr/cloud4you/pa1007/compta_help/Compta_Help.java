package fr.cloud4you.pa1007.compta_help;

import fr.cloud4you.pa1007.compta_help.builder.AccountBuilder;
import fr.cloud4you.pa1007.compta_help.controller.MainController;
import fr.cloud4you.pa1007.compta_help.creators.Account;
import fr.cloud4you.pa1007.compta_help.creators.Cash;
import fr.cloud4you.pa1007.compta_help.creators.Commands;
import fr.cloud4you.pa1007.compta_help.creators.Encrypt;
import fr.cloud4you.pa1007.compta_help.exception.NoResultFoundException;
import fr.cloud4you.pa1007.compta_help.types.CreationType;
import fr.cloud4you.pa1007.compta_help.types.MoneyType;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.*;

public class Compta_Help extends Application {

    private static Map<MoneyType, List<?>> accountsList  = new HashMap<>();
    private static MySQLConnection         sqlConnection = new MySQLConnection();
    private static Cash                    currentCash;
    private static Map<String, Account>    accountMap    = new HashMap<>();

    @Override
    public void start(Stage primaryStage) throws Exception {
        accountMap = Compta_Help.updateAccountMap();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/stage/mainScene.fxml"));
        Pane           pane                = loader.load();
        MainController controllerMainTable = loader.getController();
        Stage          stage               = new Stage();
        controllerMainTable.init();
        initStage(stage);
        stage.setScene(new Scene(pane));
        stage.setTitle("Main | Compta_Help");
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    private void initStage(Stage primaryStage) {
        primaryStage.setAlwaysOnTop(false);
        primaryStage.setResizable(true);
    }

    /**
     * To get the account list <br>
     * this map use the {@link fr.cloud4you.pa1007.compta_help.types.MoneyType} :
     * <ul>
     * <li>{@link fr.cloud4you.pa1007.compta_help.types.MoneyType#CASH}</li>
     * <li>{@link fr.cloud4you.pa1007.compta_help.types.MoneyType#BANK}</li>
     * </ul>
     * (The {@link fr.cloud4you.pa1007.compta_help.types.MoneyType#FREE} and {@link fr.cloud4you.pa1007.compta_help.types.MoneyType#OTHER} will create a {@link NullPointerException}
     *
     * @return a map of account and cash
     */
    public static Map<MoneyType, List<?>> getAccountsList() {
        return accountsList;
    }

    /**
     * To get the current cash you have
     *
     * @return {@link fr.cloud4you.pa1007.compta_help.creators.Cash} With a double money
     *
     * @throws SQLException           if the program is unable to connect to the database
     * @throws ClassNotFoundException if the driver is not here
     * @throws NoResultFoundException if there is no cash row created in your database <br> done if you have change database after lunching the program for the first time
     */
    public static Cash getCurrentCash() throws SQLException, ClassNotFoundException, NoResultFoundException {
        ResultSet rs = sqlConnection.getConnection().prepareStatement(Commands.DATA_BASE_CASH).executeQuery();

        if (!rs.next()) {
            throw new NoResultFoundException(true, null);
        }
        else {
            rs.first();
            currentCash = new Cash(rs.getDouble(" Money"));
        }


        return currentCash;
    }

    public static Map<String, Account> getAccountMap() {
        return accountMap;
    }

    /**
     * To get the updated account map
     *
     * @return The updated account map
     */
    public static Map<String, Account> updateAccountMap() {
        try {
            PreparedStatement statement = sqlConnection.getConnection().prepareStatement(Commands.DATA_BASE_ACCOUNT);
            ResultSet         resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Account temp = new AccountBuilder()
                        .setOwnerName(resultSet.getString("OwnerName"))
                        .setCreationDate(resultSet.getDate("creationDate"))
                        .setAmount(resultSet.getDouble(" Money"))
                        .setAccountName(resultSet.getString("Name")).createAccount();
                accountMap.put(temp.getAccountName(), temp);
            }


        }
        catch (SQLException | ClassNotFoundException e) {
            Compta_Help.createErrorException(e);
            return null;
        }
        return accountMap;
    }

    /**
     * Use this function for editing an Account's money in it .
     *
     * @param name  The name of the account use
     * @param money The money use/get for the transaction
     * @param type  {@link fr.cloud4you.pa1007.compta_help.types.CreationType} The type of creation for the thing
     *
     * @return The money left on the selected account
     *
     * @throws NullPointerException   throw if the account name is not found
     * @throws SQLException           throw if the application is unable to connect to the internet
     * @throws ClassNotFoundException throw if the application doesn't have the driver installed
     */
    public static double editAccountAmmount(String name, double money, CreationType type)
    throws NullPointerException, SQLException, ClassNotFoundException {
        updateAccountMap();
        switch (type) {
            case COST:
                money *= -1;
                break;
        }
        Account ac;
        try {
            ac = accountMap.get(name);
        }
        catch (NullPointerException e) {
            throw new NullPointerException("There is no account named : " + name);
        }
        ac.setAmount(ac.getAmount() + money);
        Connection        connection        = sqlConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(Commands.REPLACE_MONEY_ACCOUNT);
        preparedStatement.setDouble(1, ac.getAmount());
        preparedStatement.setString(2, ac.getAccountName());
        preparedStatement.execute();
        return ac.getAmount();

    }

    /**
     * Method to create an account
     *
     * @param name       {@link String} the name of the account
     * @param balance    {@link Double} the amount to start with this account
     * @param authorName {@link String}
     *
     * @return {@link fr.cloud4you.pa1007.compta_help.creators.Account} The account created
     *
     * @throws SQLException           if has no connection to the database
     * @throws ClassNotFoundException if doesn't have the right driver/socket for SQL
     */
    public static Account createAccount(String name, double balance, String authorName)
    throws SQLException, ClassNotFoundException {
        Account temp = new AccountBuilder()
                .setAccountName(name)
                .setAmount(balance)
                .setId(Encrypt.encryptMessage(balance, name))
                .setCreationDate(Date.from(Instant.now()))
                .setOwnerName(authorName).createAccount();

        if (accountsList.containsKey(MoneyType.BANK)) {
            List<Account> bankAccount = (List<Account>) accountsList.get(MoneyType.BANK);
            bankAccount.add(temp);
            accountsList.replace(MoneyType.BANK, bankAccount);
        }
        else {
            List<Account> bankAccount = new ArrayList<>();
            bankAccount.add(temp);
            accountsList.put(MoneyType.BANK, bankAccount);
        }

        Connection        connection        = sqlConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(Commands.CREATE_ACCOUNT);
        preparedStatement.setDouble(1, temp.getAmount());
        preparedStatement.setString(2, temp.getAccountName());
        preparedStatement.setString(3, temp.getOwnerName());
        preparedStatement.setDate(
                4,
                java.sql.Date.valueOf(temp.getCreationDate().toInstant().atZone(ZoneId.of("Europe/Paris")).toLocalDate())
        );
        preparedStatement.execute();
        return temp;
    }

    /**
     * To create a alert box with an Exception
     *
     * @param e the Exception you want to display
     */
    public static void createErrorException(Exception e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText(e.getMessage());
        StringWriter sw = new StringWriter();
        PrintWriter  pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        TextArea textArea = new TextArea(sw.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        alert.getDialogPane().setExpandableContent(textArea);
        alert.show();
    }
}
