package fr.cloud4you.pa1007.compta_help;

import fr.cloud4you.pa1007.compta_help.constants.Constants;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {

    private Connection connection;
    private boolean    connected;


    public MySQLConnection() {
    }

    /**
     * to get a new Connection
     *
     * @return Connection
     *
     * @throws SQLException           if the program is unable to connect to the database
     * @throws ClassNotFoundException if the jdbc driver is not present
     */
    public Connection getConnection() throws SQLException, ClassNotFoundException {
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        Class.forName("com.mysql.jdbc.Driver");
        if (connected && connection.isValid(1)) {

            return connection;
        }
        else {
            connection = DriverManager.getConnection(Constants.URL, Constants.USER_NAME, Constants.USER_PASSWORD);
            connected = true;
            return connection;
        }

    }
}
