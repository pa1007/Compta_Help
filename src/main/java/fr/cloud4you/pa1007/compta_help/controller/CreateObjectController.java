package fr.cloud4you.pa1007.compta_help.controller;

import fr.cloud4you.pa1007.compta_help.Compta_Help;
import fr.cloud4you.pa1007.compta_help.MySQLConnection;
import fr.cloud4you.pa1007.compta_help.builder.CostBuilder;
import fr.cloud4you.pa1007.compta_help.builder.IncomeBuilder;
import fr.cloud4you.pa1007.compta_help.creators.Commands;
import fr.cloud4you.pa1007.compta_help.creators.Cost;
import fr.cloud4you.pa1007.compta_help.creators.Encrypt;
import fr.cloud4you.pa1007.compta_help.creators.Income;
import fr.cloud4you.pa1007.compta_help.types.CreationType;
import fr.cloud4you.pa1007.compta_help.types.MoneyType;
import fr.cloud4you.pa1007.compta_help.types.UsageType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CreateObjectController {

    public  TextField              amountText;
    public  TextField              nameText;
    public  TextField              descText;
    public  ChoiceBox<String>      moneyChoices;
    public  DatePicker             datePicker;
    public  Text                   titleText;
    public  Text                   errorText;
    public  ChoiceBox<String>      usageChoices;
    public  ChoiceBox<String>      bankNameChoice;
    public  Text                   nameBankText;
    private CreationType           type;
    private Cost                   cost;
    private Income                 income;
    private MySQLConnection        mySQLConnection = new MySQLConnection();
    private ObservableList<String> accountsList    = FXCollections.observableArrayList();

    @FXML
    public void keyAmountBox(KeyEvent keyEvent) {
        if (keyEvent.getCharacter().equals(".")) {
            return;
        }
        try {
            Integer.valueOf(keyEvent.getCharacter());
        }
        catch (NumberFormatException e) {
            keyEvent.consume();
        }

    }

    @FXML
    public void createItem(ActionEvent event) {
        if (type.equals(CreationType.COST)) {
            if (usageChoices.getValue().equals("")) {
                return;
            }
        }
        if (datePicker.getEditor().getText().equals("")) {
            errorText.setText("Please enter a date !");
            return;
        }
        if (amountText.equals("")) {
            errorText.setText("Amount value is null");
            return;
        }
        if (nameText.equals("")) {
            errorText.setText("Name is empty");
            return;
        }
        if (descText.equals("")) {
            errorText.setText("Description is empty");
            return;
        }
        String val;
        try {
            val = moneyChoices.getValue();
        }
        catch (NullPointerException e) {
            errorText.setText("Please choose a money type !");
            return;
        }
        if (val.equals("BANK")) {
            try {
                bankNameChoice.getValue();
            }
            catch (NullPointerException e) {
                errorText.setText("Please choose a Account Name !");
                return;
            }
        }
        String[] date     = datePicker.getEditor().getText().split("/");
        Calendar calendar = new GregorianCalendar();
        calendar.set(Integer.valueOf(date[2]), Integer.valueOf(date[1]), Integer.valueOf(date[0]));
        boolean created = false;
        if (income == null && cost == null) {
            if (type.equals(CreationType.COST)) {
                cost =
                        new CostBuilder()
                                .setAmount(Double.valueOf(amountText.getText()))
                                .setDescription(descText.getText())
                                .setName(nameText.getText())
                                .setYear(Integer.valueOf(date[2]))
                                .setWeek(calendar.get(Calendar.WEEK_OF_YEAR))
                                .setMonth(Integer.valueOf(date[1]))
                                .setDay(Integer.valueOf(date[0]))
                                .setMoneyType(MoneyType.getType(moneyChoices.getValue()))
                                .setId(Encrypt.encryptMessage(
                                        calendar.get(Calendar.WEEK_OF_YEAR) * Double.valueOf(amountText.getText()),
                                        nameText.getText()
                                ))
                                .setUsageType(UsageType.valueOf(usageChoices.getValue()))
                                .setBankName((val.equals("BANK") ? bankNameChoice.getValue() : null))
                                .createCost();
                created = true;
            }
            else {
                income =
                        new IncomeBuilder().setAmount(Double.valueOf(amountText.getText()))
                                .setDescription(descText.getText())
                                .setName(nameText.getText())
                                .setYear(Integer.valueOf(date[2]))
                                .setWeek(calendar.get(Calendar.WEEK_OF_YEAR))
                                .setMonth(Integer.valueOf(date[1]))
                                .setDay(Integer.valueOf(date[0]))
                                .setMoneyType(MoneyType.getType(moneyChoices.getValue()))
                                .setId(Encrypt.encryptMessage(
                                        calendar.get(Calendar.WEEK_OF_YEAR) * Double.valueOf(amountText.getText()),
                                        nameText.getText()
                                ))
                                .setAccountName((val.equals("BANK") ? bankNameChoice.getValue() : null))
                                .createIncome();
                created = true;
            }

        }
        else if (income != null) {
            double oldVal = income.getAmount();
            income.setAmount(Double.valueOf(amountText.getText()));
            income.setDescription(descText.getText());
            income.setName(nameText.getText());
            income.setYear(Integer.valueOf(date[2]));
            income.setWeek(calendar.get(Calendar.WEEK_OF_YEAR));
            income.setMonth(Integer.valueOf(date[1]));
            income.setDay(Integer.valueOf(date[0]));
            income.setMoneyType(MoneyType.getType(moneyChoices.getValue()));
            income.setBankName((val.equals("BANK") ? bankNameChoice.getValue() : null));
            type = CreationType.INCOME;
            if (editAcc(oldVal, income.getBankName(), income.getAmount())) {
                return;
            }

        }
        else {
            double oldVal = cost.getAmount();
            cost.setAmount(Double.valueOf(amountText.getText()));
            cost.setDescription(descText.getText());
            cost.setName(nameText.getText());
            cost.setYear(Integer.valueOf(date[2]));
            cost.setWeek(calendar.get(Calendar.WEEK_OF_YEAR));
            cost.setMonth(Integer.valueOf(date[1]));
            cost.setDay(Integer.valueOf(date[0]));
            cost.setMoneyType(MoneyType.getType(moneyChoices.getValue()));
            cost.setUsageType(UsageType.valueOf(usageChoices.getValue()));
            cost.setBankName((val.equals("BANK") ? bankNameChoice.getValue() : null));
            type = CreationType.COST;
            if (editAcc(oldVal, cost.getBankName(), cost.getAmount())) {
                return;
            }
        }
        PreparedStatement statement;
        Connection        connection;
        try {
            connection = mySQLConnection.getConnection();
        }
        catch (Exception e) {
            Compta_Help.createErrorException(e);
            return;
        }
        if (created) {
            try {
                if (type.equals(CreationType.INCOME)) {
                    statement = connection.prepareStatement(Commands.CREATE_INCOME);

                    statement.setDouble(1, income.getAmount());
                    statement.setString(2, income.getName());
                    statement.setString(3, income.getDescription());
                    statement.setInt(4, income.getMonth());
                    statement.setInt(5, income.getWeek());
                    statement.setInt(6, income.getYear());
                    statement.setInt(7, MoneyType.getValue(income.getMoneyType()));
                    statement.setInt(8, income.getDay());
                    statement.setString(9, income.getId());
                    statement.execute();

                    if (income.getMoneyType().equals(MoneyType.BANK)) {
                        Compta_Help.editAccountAmmount(income.getBankName(), income.getAmount(), CreationType.INCOME);
                    }
                }
                else {
                    statement = connection.prepareStatement(Commands.CREATE_COST);

                    statement.setDouble(1, cost.getAmount());
                    statement.setString(2, cost.getName());
                    statement.setString(3, cost.getDescription());
                    statement.setInt(4, cost.getMonth());
                    statement.setInt(5, cost.getWeek());
                    statement.setInt(6, cost.getYear());
                    statement.setInt(7, MoneyType.getValue(cost.getMoneyType()));
                    statement.setInt(8, cost.getDay());
                    statement.setString(9, cost.getId());
                    statement.setString(10, cost.getUsageType().name());
                    statement.execute();

                    if (cost.getMoneyType().equals(MoneyType.BANK)) {
                        Compta_Help.editAccountAmmount(cost.getBankName(), cost.getAmount(), CreationType.COST);
                    }
                }
            }
            catch (SQLException | NullPointerException | ClassNotFoundException e) {
                Compta_Help.createErrorException(e);
                errorText.setText("ERROR WHILE EXECUTING CREATE COMMANDS SEE LOG");
            }
        }
        else {
            try {
                if (type.equals(CreationType.INCOME)) {
                    statement = connection.prepareStatement(Commands.REPLACE_INCOME);

                    statement.setDouble(1, income.getAmount());
                    statement.setString(2, income.getName());
                    statement.setString(3, income.getDescription());
                    statement.setInt(4, income.getMonth());
                    statement.setInt(5, income.getWeek());
                    statement.setInt(6, income.getYear());
                    statement.setInt(7, MoneyType.getValue(income.getMoneyType()));
                    statement.setInt(8, income.getDay());
                    statement.setString(9, income.getId());
                    statement.execute();
                }
                else {
                    statement = connection.prepareStatement(Commands.REPLACE_COST);

                    statement.setDouble(1, cost.getAmount());
                    statement.setString(2, cost.getName());
                    statement.setString(3, cost.getDescription());
                    statement.setInt(4, cost.getMonth());
                    statement.setInt(5, cost.getWeek());
                    statement.setInt(6, cost.getYear());
                    statement.setInt(7, MoneyType.getValue(cost.getMoneyType()));
                    statement.setInt(8, cost.getDay());
                    statement.setString(9, cost.getId());
                    statement.setString(10, cost.getUsageType().name());
                    statement.execute();
                }

                closeStage(event);
            }
            catch (SQLException e) {
                Compta_Help.createErrorException(e);
                errorText.setText("ERROR WHILE EXECUTING REPLACE COMMANDS SEE LOG");
            }
        }
    }

    void init(CreationType type) {
        this.type = type;
        titleText.setText(type.name() + " :");
        if (type.equals(CreationType.INCOME)) {
            usageChoices.setDisable(true);
        }
        usageChoices.setOnMouseClicked(
                event -> {
                    try {
                        String st = usageChoices.getValue();
                        if (st.equals("BANK")) {
                            bankNameChoice.setVisible(true);
                            nameBankText.setVisible(true);
                        }
                        else {
                            bankNameChoice.setVisible(false);
                            nameBankText.setVisible(false);
                        }
                    }
                    catch (Exception ignored) {

                    }
                }
        );
        accountsList.addAll(Compta_Help.updateAccountMap().keySet());
        bankNameChoice.setItems(accountsList);
        income = null;
        cost = null;
    }

    void load(Income income) {
        titleText.setText("Income :");
        this.income = income;
        this.type = CreationType.INCOME;
        amountText.setText(String.valueOf(income.getAmount()));
        nameText.setText(income.getName());
        descText.setText(income.getDescription());
        datePicker.getEditor().setText(income.getDay() + "/" + income.getMonth() + "/" + income.getYear());
        moneyChoices.setValue(income.getMoneyType().name());
        usageChoices.setDisable(true);
        accountsList.addAll(Compta_Help.updateAccountMap().keySet());
        bankNameChoice.setItems(accountsList);
        this.cost = null;

    }

    void load(Cost cost) {
        this.cost = cost;
        this.type = CreationType.COST;
        titleText.setText("Cost :");
        amountText.setText(String.valueOf(cost.getAmount()));
        nameText.setText(cost.getName());
        descText.setText(cost.getDescription());
        datePicker.getEditor().setText(cost.getDay() + "/" + cost.getMonth() + "/" + cost.getYear());
        moneyChoices.setValue(cost.getMoneyType().name());
        usageChoices.setValue(cost.getUsageType().name());
        accountsList.addAll(Compta_Help.updateAccountMap().keySet());
        bankNameChoice.setItems(accountsList);
        this.income = null;
    }

    private boolean editAcc(double oldVal, String bankName, double amount) {
        try {
            Compta_Help.editAccountAmmount(bankName, oldVal - amount, type);
        }
        catch (SQLException | ClassNotFoundException e) {
            Compta_Help.createErrorException(e);
            e.printStackTrace();
            return true;
        }
        return false;
    }

    private void closeStage(ActionEvent event) {
        Node  source = (Node) event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
