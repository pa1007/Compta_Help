package fr.cloud4you.pa1007.compta_help.controller;

import fr.cloud4you.pa1007.compta_help.Compta_Help;
import fr.cloud4you.pa1007.compta_help.MySQLConnection;
import fr.cloud4you.pa1007.compta_help.builder.TransactionBuilder;
import fr.cloud4you.pa1007.compta_help.creators.Account;
import fr.cloud4you.pa1007.compta_help.creators.Cash;
import fr.cloud4you.pa1007.compta_help.creators.Transaction;
import fr.cloud4you.pa1007.compta_help.exception.NoResultFoundException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

public class TransactionBoxController {

    public  ToggleButton           mainToggleButton;
    public  TextField              amountField;
    public  ChoiceBox<String>      bankAccountName;
    public  Map<String, Account>   accountMap;
    private MySQLConnection        mySQLConnection = new MySQLConnection();
    private boolean                right           = false;
    private ObservableList<String> accountsList    = FXCollections.observableArrayList();

    public void init() {
        accountMap = Compta_Help.updateAccountMap();
        accountsList.addAll(accountMap.keySet());

        bankAccountName.setItems(accountsList);
    }

    @FXML
    public void changeButtonOrientation(ActionEvent event) {
        mainToggleButton.getStyleClass().clear();
        if (right) {
            mainToggleButton.getStyleClass().add("togglebuttonleft");
            right = false;
        }
        else {
            mainToggleButton.getStyleClass().add("togglebuttonright");
            right = true;
        }
    }

    @FXML
    public void keyTyped(KeyEvent keyEvent) {
        try {
            Integer.valueOf(keyEvent.getCharacter());
        }
        catch (NumberFormatException e) {
            keyEvent.consume();
        }
    }

    public void doneTransactionHandler(ActionEvent event) {
        if (amountField.getText().equals("")) {
            return;
        }
        String name;
        try {
            bankAccountName.getValue().toLowerCase();
            name = bankAccountName.getValue();
        }
        catch (NullPointerException e) {
            return;
        }
        Cash cash;

        try {
            cash = Compta_Help.getCurrentCash();
        }
        catch (SQLException | ClassNotFoundException | NoResultFoundException e) {
            Compta_Help.createErrorException(e);
            return;
        }
        double money = Double.parseDouble(amountField.getText());
        Transaction transaction = new TransactionBuilder()
                .setTransactionDate(Date.from(Instant.now()))
                .setAccountMoneyBefore(accountMap.get(name).getAmount())
                .setAccountName(bankAccountName.getValue())
                .setCashBefore(cash.getAmount())
                .setAmount(money)
                .setCashToAccount(!right).createTransaction();

        Account temp = accountMap.get(name);
        if (right) {
            temp.setAmount(temp.getAmount() - money);
            cash.setAmount(cash.getAmount() + money);

        }
        else {
            temp.setAmount(temp.getAmount() + money);
            cash.setAmount(cash.getAmount() - money);
        }
        temp.setLastTransaction(transaction);
        cash.setLastTransaction(transaction);

        //todo save transaction on file

    }

    private void closeStage(ActionEvent event) {
        Node  source = (Node) event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
