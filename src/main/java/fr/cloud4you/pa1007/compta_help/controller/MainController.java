package fr.cloud4you.pa1007.compta_help.controller;

import fr.cloud4you.pa1007.compta_help.Compta_Help;
import fr.cloud4you.pa1007.compta_help.MySQLConnection;
import fr.cloud4you.pa1007.compta_help.builder.CostBuilder;
import fr.cloud4you.pa1007.compta_help.builder.IncomeBuilder;
import fr.cloud4you.pa1007.compta_help.creators.Cash;
import fr.cloud4you.pa1007.compta_help.creators.Commands;
import fr.cloud4you.pa1007.compta_help.creators.Cost;
import fr.cloud4you.pa1007.compta_help.creators.Income;
import fr.cloud4you.pa1007.compta_help.exception.NoResultFoundException;
import fr.cloud4you.pa1007.compta_help.types.CreationType;
import fr.cloud4you.pa1007.compta_help.types.MoneyType;
import fr.cloud4you.pa1007.compta_help.types.UsageType;
import fr.cloud4you.pa1007.compta_help.wishlist.controller.MainWishListController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.annotation.Nullable;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class MainController {

    public        TextField                  textLast;
    public        ChoiceBox<String>          accountName;
    private       MySQLConnection            mySQLConnection = new MySQLConnection();
    private       ObservableList<Income>     dataIncome      = FXCollections.observableArrayList();
    private       ObservableList<Cost>       dataCost        = FXCollections.observableArrayList();
    private final Map<Integer, List<Cost>>   monthYearCost   = new HashMap<>();
    private final Map<Integer, List<Income>> monthYearIncome = new HashMap<>();
    @FXML
    private       ImageView                  statusImage;
    @FXML
    private       TableView<Cost>            costTable;
    @FXML
    private       TableView<Income>          incomeTable;
    @FXML
    private       Label                      leftFootText;
    @FXML
    private       Button                     buttonUpdateTable;
    @FXML
    private       MenuBar                    menuBar;
    @FXML
    private       ProgressBar                dateProgressBar;
    @FXML
    private       Button                     buttonRecalculate;
    @FXML
    private       Text                       mainText;
    @FXML
    private       Font                       x1;
    @FXML
    private       Color                      x2;
    @FXML
    private       Font                       x3;
    @FXML
    private       Label                      rightFootText;
    @FXML
    private       Color                      x4;
    @FXML
    private       Circle                     redIndicator;
    @FXML
    private       Circle                     waitingIndicator;
    @FXML
    private       Circle                     greenIndicator;

    public void init() {
        updateProgressBar();
        dateProgressBar.setOnMouseClicked(event -> updateProgressBar());
        Statement statement;
        waitingIndicator.setFill(Color.ORANGE);
        try {
            Connection connection = mySQLConnection.getConnection();
            statement = connection.createStatement();
            rightFootText.setText("Connected");
            greenIndicator.setFill(Color.LIMEGREEN);
        }
        catch (SQLException | ClassNotFoundException e) {
            createAlertERROR(e, "Network error");
            rightFootText.setText("Network error");
            return;
        }
        waitingIndicator.setFill(Color.rgb(199, 199, 199));
        updateCost(statement);
        updateIncome(statement);
        calculate(mainText);

    }

    public void costCreateHandler(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/create.fxml"));
        Pane                   page      = loader.load();
        CreateObjectController contoller = loader.getController();
        contoller.init(CreationType.COST);
        Stage stage = new Stage();
        stage.setScene(new Scene(page));
        stage.setResizable(false);
        //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
        stage.setTitle("Create a " + CreationType.COST + " | Compta Help");
        stage.showAndWait();
    }

    public void incoCreateHandler(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/create.fxml"));
        Pane                   page      = loader.load();
        CreateObjectController contoller = loader.getController();
        contoller.init(CreationType.INCOME);
        Stage stage = new Stage();
        stage.setScene(new Scene(page));
        stage.setResizable(false);
        //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
        stage.setTitle("Create a " + CreationType.INCOME + " | Compta Help");
        stage.showAndWait();
    }

    public void costEditHandler(ActionEvent event) throws IOException {

        try {
            Cost       cost   = costTable.getSelectionModel().getSelectedItem();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/create.fxml"));
            Pane                   page      = loader.load();
            CreateObjectController contoller = loader.getController();
            contoller.load(cost);
            Stage stage = new Stage();
            stage.setScene(new Scene(page));
            stage.setResizable(false);
            //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
            stage.setTitle("Edit a " + CreationType.INCOME + " | Compta Help");
            stage.showAndWait();
        }
        catch (NullPointerException ignored) {

        }

    }

    public void incoEditHandler(ActionEvent event) throws IOException {
        try {
            Income     income = incomeTable.getSelectionModel().getSelectedItem();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/create.fxml"));
            Pane                   page      = loader.load();
            CreateObjectController contoller = loader.getController();
            contoller.load(income);
            Stage stage = new Stage();
            stage.setScene(new Scene(page));
            stage.setResizable(false);
            //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
            stage.setTitle("Edit a " + CreationType.INCOME + " | Compta Help");
            stage.showAndWait();
        }
        catch (NullPointerException ignored) {
        }

    }

    public void newTransaction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/transactionBox.fxml"));
        Pane                     page      = loader.load();
        TransactionBoxController contoller = loader.getController();
        contoller.init();
        Stage stage = new Stage();
        stage.setScene(new Scene(page));
        stage.setResizable(false);
        //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
        stage.setTitle("Create a new Transaction | Compta Help");
        stage.showAndWait();

    }

    public void changeCash(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/cashEditDialog.fxml"));
        Pane           page       = loader.load();
        CashEditDialog controller = loader.getController();
        Stage          stage      = new Stage();
        Cash           cash;
        try {
            cash = Compta_Help.getCurrentCash();
            controller.setCash(cash);
        }
        catch (SQLException | ClassNotFoundException | NoResultFoundException e) {
            Compta_Help.createErrorException(e);
            return;
        }
        stage.setScene(new Scene(page));
        stage.setResizable(false);
        //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
        stage.setTitle("Create a new Account | Compta Help");
        stage.showAndWait();
        try {
            PreparedStatement ps = mySQLConnection.getConnection().prepareStatement(Commands.REPLACE_MONEY_CASH);
            ps.setDouble(1, cash.getAmount());
            ps.execute();
        }
        catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Compta_Help.createErrorException(e);
        }
    }

    public void changeBank(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/accountCreation.fxml"));
        Pane                      page      = loader.load();
        AccountCreationController contoller = loader.getController();
        Stage                     stage     = new Stage();
        stage.setScene(new Scene(page));
        stage.setResizable(false);
        //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
        stage.setTitle("Create a new Account | Compta Help");
        stage.showAndWait();
    }

    public void updateIncome(Statement statement) {
        dataIncome.clear();
        ResultSet resultSet1;
        try {
            resultSet1 = statement.executeQuery(Commands.DATA_BASE_INCOME);
            while (resultSet1.next()) {
                Income tempIncome = new IncomeBuilder()
                        .setAmount(resultSet1.getDouble("Amount"))
                        .setDescription(resultSet1.getString("Description"))
                        .setName(resultSet1.getString("Name"))
                        .setYear(resultSet1.getInt("Year"))
                        .setWeek(resultSet1.getInt("Week"))
                        .setMonth(resultSet1.getInt("Month"))
                        .setDay(resultSet1.getInt("Day"))
                        .setMoneyType(MoneyType.valueOf(resultSet1.getInt("Type")))
                        .createIncome();
                int id = tempIncome.getMonth() + tempIncome.getYear();
                if (monthYearIncome.containsKey(id)) {
                    List<Income> tempList = monthYearIncome.get(id);
                    if (!tempList.contains(tempIncome)) {
                        tempList.add(tempIncome);
                    }
                    monthYearIncome.replace(id, tempList);
                }
                else {
                    List<Income> tempList = new ArrayList<>();
                    if (!tempList.contains(tempIncome)) {
                        tempList.add(tempIncome);
                    }
                    monthYearIncome.put(id, tempList);
                }

                dataIncome.add(tempIncome);
            }
            incomeTable.setItems(dataIncome);
            greenIndicator.setFill(Color.LIMEGREEN);
        }
        catch (SQLException e) {
            createAlertERROR(e, "Income");
        }
    }

    public void updateCost(Statement statement) {
        dataCost.clear();
        ResultSet resultSet2;
        try {
            resultSet2 = statement.executeQuery(Commands.DATA_BASE_COST);
            while (resultSet2.next()) {
                Cost tempCost =
                        new CostBuilder()
                                .setAmount(resultSet2.getDouble("Price"))
                                .setDescription(resultSet2.getString("Description"))
                                .setName(resultSet2.getString("Name"))
                                .setYear(resultSet2.getInt("Year"))
                                .setWeek(resultSet2.getInt("Week"))
                                .setMonth(resultSet2.getInt("Month"))
                                .setDay(resultSet2.getInt("Day"))
                                .setMoneyType(MoneyType.valueOf(resultSet2.getInt("Type")))
                                .setId(resultSet2.getString("ID"))
                                .setUsageType(UsageType.valueOf(String.valueOf(resultSet2.getObject("UsageType"))))
                                .createCost();
                int id = tempCost.getMonth() + tempCost.getYear();
                if (monthYearCost.containsKey(id)) {
                    List<Cost> tempList = monthYearCost.get(id);
                    if (!tempList.contains(tempCost)) {
                        tempList.add(tempCost);
                    }
                    monthYearCost.replace(id, tempList);
                }
                else {
                    List<Cost> tempList = new ArrayList<>();
                    if (!tempList.contains(tempCost)) {
                        tempList.add(tempCost);
                    }
                    monthYearCost.put(id, tempList);
                }

                dataCost.add(tempCost);
            }
            costTable.setItems(dataCost);
            greenIndicator.setFill(Color.LIMEGREEN);
        }
        catch (SQLException e) {
            createAlertERROR(e, "Cost");
        }
    }

    @FXML
    public void keyTyped(KeyEvent keyEvent) {
        try {
            Integer.valueOf(keyEvent.getCharacter());
        }
        catch (NumberFormatException e) {
            keyEvent.consume();
        }
    }

    public void pdfLast(ActionEvent event) {

    }

    public void printLast(ActionEvent event) {

    }

    public void pdfCurrent(ActionEvent event) {

    }

    public void printCurrent(ActionEvent event) {

    }

    public void undoTransaction(ActionEvent event) {

    }

    public void openWLHandler(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/wishlist/stage/mainStage.fxml"));
        Pane                   page      = loader.load();
        MainWishListController contoller = loader.getController();
        Stage                  stage     = new Stage();
        stage.setScene(new Scene(page));
        stage.setResizable(false);
        //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
        stage.setTitle("Wish List| Compta Help");
        stage.showAndWait();
    }

    public void seeBank(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fr/cloud4you/pa1007/compta_help/alertbox/seeBankAlertBox.fxml"));
        Pane                     page      = loader.load();
        SeeBankController contoller = loader.getController();
        contoller.init();
        Stage stage = new Stage();
        stage.setScene(new Scene(page));
        stage.setResizable(false);
        //stage.getIcons().add(new Image(Compta_Help.class.getResourceAsStream("/images/AppIcon.png")));
        stage.setTitle("See all the bank information| Compta Help");
        stage.showAndWait();
    }

    private void createAlertERROR(Exception ex, @Nullable String exceptionLocation) {
        Compta_Help.createErrorException(ex);
        waitingIndicator.setFill(Color.rgb(199, 199, 199));
        redIndicator.setFill(Color.RED);
        waitingIndicator.setFill(Color.rgb(199, 199, 199));
        leftFootText.setText("Error while loading " + ((exceptionLocation != null) ? exceptionLocation : ": see log"));
    }

    @FXML
    private void updateTableHandler(ActionEvent event) {
        waitingIndicator.setFill(Color.ORANGE);
        Statement statement;
        try {
            Connection connection = mySQLConnection.getConnection();
            statement = connection.createStatement();
            rightFootText.setText("Connected");
            greenIndicator.setFill(Color.LIMEGREEN);
        }
        catch (Exception e) {
            e.printStackTrace();
            rightFootText.setText("Network error");
            redIndicator.setFill(Color.RED);
            return;
        }
        updateCost(statement);
        updateIncome(statement);
        waitingIndicator.setFill(Color.gray(0.8));

    }

    @FXML
    private void recalculateButtonAction(ActionEvent event) {
        calculate(mainText);
    }

    private void calculate(Text mainText) {
        double totalCost   = 0.0;
        double totalIncome = 0.0;
        int id = Calendar.getInstance().get(Calendar.MONTH)
                 + Calendar.getInstance().get(GregorianCalendar.YEAR) + 1;
        try {
            for (Cost cost : monthYearCost.get(id)) {
                if (!cost.getMoneyType().equals(MoneyType.FREE)) {
                    totalCost += cost.getAmount();
                }

            }
        }
        catch (NullPointerException ignored) {
        }
        try {
            for (Income income : monthYearIncome.get(id)) {
                totalIncome += income.getAmount();
            }
        }
        catch (NullPointerException ignored) {
        }
        double result = totalIncome - totalCost;

        if (result > 0) {
            mainText.setFill(Color.GREEN);
        }
        else if (result < 0) {
            mainText.setFill(Color.GRAY);
        }
        else {
            mainText.setFill(Color.RED);
        }

        mainText.setText(String.valueOf(result));
    }

    private void updateProgressBar() {
        double progress = (
                (double) Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                / (double) Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)
        );
        dateProgressBar.setProgress(progress);
    }

}
