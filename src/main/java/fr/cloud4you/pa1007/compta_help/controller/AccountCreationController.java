package fr.cloud4you.pa1007.compta_help.controller;

import fr.cloud4you.pa1007.compta_help.Compta_Help;
import fr.cloud4you.pa1007.compta_help.creators.Account;
import fr.cloud4you.pa1007.compta_help.exception.AlreadyExistingException;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import java.sql.SQLException;
import java.util.Map;

public class AccountCreationController {

    public TextField            amountField;
    public TextField            nameField;
    public TextField            ownerField;
    public Map<String, Account> accountMap = Compta_Help.getAccountMap();

    public void numberField(KeyEvent keyEvent) {
        try {
            Integer.valueOf(keyEvent.getCharacter());
        }
        catch (NumberFormatException e) {
            keyEvent.consume();
        }
    }

    public void buttonHandler(ActionEvent event) {
        if (amountField.getText().equals("")) {
            return;
        }
        if (nameField.getText().equals("")) {
            return;
        }
        if (ownerField.getText().equals("")) {
            return;
        }

        if (!accountMap.containsKey(nameField.getText())) {
            try {
                Compta_Help.createAccount(
                        nameField.getText(),
                        Double.parseDouble(amountField.getText()),
                        ownerField.getText()
                );
            }
            catch (SQLException | ClassNotFoundException e) {
                Compta_Help.createErrorException(e);
                return;
            }

            closeStage(event);
        }
        else {
            Compta_Help.createErrorException(new AlreadyExistingException(nameField.getText()));
        }
    }

    private void closeStage(ActionEvent event) {
        Node  source = (Node) event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
