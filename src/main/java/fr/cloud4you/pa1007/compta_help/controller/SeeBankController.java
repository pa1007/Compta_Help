package fr.cloud4you.pa1007.compta_help.controller;

import fr.cloud4you.pa1007.compta_help.Compta_Help;
import fr.cloud4you.pa1007.compta_help.creators.Account;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.text.Text;
import java.util.Map;

public class SeeBankController {

    public  ChoiceBox              bankAccountName;
    public  Text                   accountName;
    public  Text                   accountOwner;
    public  Text                   accountAmount;
    public  Map<String, Account>   accountMap;
    public  String                 bankName;
    private ObservableList<String> accountsList = FXCollections.observableArrayList();


    public SeeBankController() {
    }

    public void init() {
        accountMap = Compta_Help.updateAccountMap();
        accountsList.addAll(accountMap.keySet());

        bankAccountName.setItems(accountsList);
        bankAccountName.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            bankName = accountsList.get((Integer) newValue);
            Account ac = accountMap.get(bankName);

            accountName.setText(ac.getAccountName());
            accountOwner.setText(ac.getOwnerName());
            accountAmount.setText(String.valueOf(ac.getAmount()));

        });
    }
}
