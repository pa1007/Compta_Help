package fr.cloud4you.pa1007.compta_help.controller;

import fr.cloud4you.pa1007.compta_help.creators.Cash;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class CashEditDialog {

    public  TextField numberField;
    private Cash      cash;

    public void setCash(Cash cash) {
        numberField.setText(String.valueOf(cash.getAmount()));
        this.cash = cash;
    }

    public void doneHandler(ActionEvent event) {
        if (numberField.getText().equals("")) {
            return;
        }
        cash.setAmount(Double.parseDouble(numberField.getText()));
        closeStage(event);
    }

    public void keyTyped(KeyEvent keyEvent) {
        try {
            Integer.valueOf(keyEvent.getCharacter());
        }
        catch (NumberFormatException e) {
            keyEvent.consume();
        }
    }

    private void closeStage(ActionEvent event) {
        Node  source = (Node) event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
