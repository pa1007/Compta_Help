package fr.cloud4you.pa1007.compta_help.creators;

import java.util.Date;
import java.util.Objects;

public class Account {


    /**
     * The last transaction on the account from the cash.
     *
     * @since 1.0
     */
    private Transaction lastTransaction;

    /**
     * The amount in the bank account.
     *
     * @since 1.0
     */
    private double amount;

    /**
     * A simple way to find the good account.
     *
     * @since 1.0
     */
    private String accountName;


    /**
     * The Date of the creation.
     *
     * @since 1.0
     */
    private Date creationDate;

    /**
     * The id of the account.
     *
     * @since 1.0
     */
    private String id;

    /**
     * The name(s) of the owner.
     *
     * @since 1.0
     */
    private String ownerName;


    public Account(Date creationDate, double amount, String accountName, String id, String ownerName) {
        this.creationDate = creationDate;
        this.amount = amount;
        this.accountName = accountName;
        this.id = id;
        this.ownerName = ownerName;
    }

    /**
     * @return The Date of the creation.
     * @since 1.0
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Sets the <code>creationDate</code> field.
     *
     * @param creationDate The Date of the creation.
     * @since 1.0
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return The id of the account.
     * @since 1.0
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the <code>id</code> field.
     *
     * @param id The id of the account.
     * @since 1.0
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name(s) of the owner.
     * @since 1.0
     */
    public String getOwnerName() {
        return this.ownerName;
    }

    /**
     * Sets the <code>ownerName</code> field.
     *
     * @param ownerName The name(s) of the owner.
     * @since 1.0
     */
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }


    /**
     * @return A simple way to find the good account.
     * @since 1.0
     */
    public String getAccountName() {
        return this.accountName;
    }

    /**
     * Sets the <code>accountName</code> field.
     *
     * @param accountName A simple way to find the good account.
     * @since 1.0
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * @return The last transaction on the account from the cash.
     * @since 1.0
     */
    public Transaction getLastTransaction() {
        return this.lastTransaction;
    }

    /**
     * Sets the <code>lastTransaction</code> field.
     *
     * @param lastTransaction The last transaction on the account from the cash.
     * @since 1.0
     */
    public void setLastTransaction(Transaction lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    /**
     * @return The amount in the bank account.
     * @since 1.0
     */
    public double getAmount() {
        return this.amount;
    }

    /**
     * Sets the <code>amount</code> field.
     *
     * @param amount The amount in the bank account.
     * @since 1.0
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }
        Account account = (Account) o;
        return Double.compare(account.amount, amount) == 0 &&
               Objects.equals(lastTransaction, account.lastTransaction) &&
               Objects.equals(accountName, account.accountName) &&
               Objects.equals(id, account.id) &&
               Objects.equals(ownerName, account.ownerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastTransaction, amount, accountName, id, ownerName);
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("lastTransaction", lastTransaction)
                .add("amount", amount)
                .add("accountName", accountName)
                .add("id", id)
                .add("ownerName", ownerName)
                .toString();
    }
}
