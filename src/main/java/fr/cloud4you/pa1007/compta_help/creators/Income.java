package fr.cloud4you.pa1007.compta_help.creators;

import fr.cloud4you.pa1007.compta_help.types.MoneyType;
import java.util.Objects;

public class Income {


    /**
     * The amount of the income.
     *
     * @since 1.0
     */
    private double amount;

    /**
     * The name of the bank account used.
     *
     * @since 1.0
     */
    private String bankName;

    /**
     * The type of money that has been send.
     *
     * @since 1.0
     */
    private MoneyType moneyType;

    /**
     * a description of the income.
     *
     * @since 1.0
     */
    private String description;

    /**
     * The name of the Income.
     *
     * @since 1.0
     */
    private String name;

    /**
     * The year of the transaction.
     *
     * @since 1.0
     */
    private int year;

    /**
     * The week of the month .
     *
     * @since 1.0
     */
    private int week;

    /**
     * get the month number between 0 and 52.
     *
     * @since 1.0
     */
    private int month;

    /**
     * The day of the month.
     *
     * @since 1.0
     */
    private int day;

    /**
     * The id of tge Income.
     * create with the key = weekNumber*amount
     * message Name
     *
     * @since 1.0
     */
    private String id;

    /**
     * <H2 >The main constructor</H2>
     *
     * @param amount      {@link Double} the amount of the cost
     * @param description {@link String} Description of the cost
     * @param name        {@link String} Name of the cost
     * @param year        {@link Integer} Year number (e.g 2018)
     * @param week        {@link Integer} Week number (e.g 54)
     * @param month       {@link Integer} month number (e.g 12)
     * @param moneyType   {@link fr.cloud4you.pa1007.compta_help.types.MoneyType} The type of the money use
     * @param day         {@link Integer} the number of the day (e.g 25)
     * @param id          {@link String} autogenerated id see {@link fr.cloud4you.pa1007.compta_help.creators.Encrypt#encryptMessage(Double, String)}
     */
    public Income(
            double amount,
            String description,
            String name,
            int year,
            int week,
            int month,
            int day,
            MoneyType moneyType,
            String id
    ) {
        this.amount = amount;
        this.description = description;
        this.name = name;
        this.year = year;
        this.week = week;
        this.month = month;
        this.day = day;
        this.moneyType = moneyType;
        this.id = id;
    }

    /**
     * The constructor with a given account name <br>
     *
     * @param amount      {@link Double} the amount of the cost
     * @param description {@link String} Description of the cost
     * @param name        {@link String} Name of the cost
     * @param year        {@link Integer} Year number (e.g 2018)
     * @param week        {@link Integer} Week number (e.g 54)
     * @param month       {@link Integer} month number (e.g 12)
     * @param moneyType   {@link fr.cloud4you.pa1007.compta_help.types.MoneyType} The type of the money use
     * @param day         {@link Integer} the number of the day (e.g 25)
     * @param id          {@link String} autogenerated id see {@link fr.cloud4you.pa1007.compta_help.creators.Encrypt#encryptMessage(Double, String)}
     * @param accountName {@link String} The name of the account use
     */
    public Income(
            double amount,
            String description,
            String name,
            int year,
            int week,
            int month,
            int day,
            MoneyType moneyType,
            String id,
            String accountName
    ) {
        this.amount = amount;
        this.description = description;
        this.name = name;
        this.year = year;
        this.week = week;
        this.month = month;
        this.day = day;
        this.moneyType = moneyType;
        this.id = id;
        this.bankName = accountName;
    }

    /**
     * @return The name of the bank account used.
     * @since 1.0
     */
    public String getBankName() {
        return this.bankName;
    }

    /**
     * Sets the <code>bankName</code> field.
     *
     * @param bankName The name of the bank account used.
     * @since 1.0
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return The id of tge Income.
     * @since 1.0
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the <code>id</code> field.
     *
     * @param id The id of tge Income.
     * @since 1.0
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The day of the month.
     * @since 1.0
     */
    public int getDay() {
        return this.day;
    }

    /**
     * Sets the <code>day</code> field.
     *
     * @param day The day of the month.
     * @since 1.0
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * @return The type of money that has been send.
     * @since 1.0
     */
    public MoneyType getMoneyType() {
        return this.moneyType;
    }

    /**
     * Sets the <code>moneyType</code> field.
     *
     * @param moneyType The type of money that has been send.
     * @since 1.0
     */
    public void setMoneyType(MoneyType moneyType) {
        this.moneyType = moneyType;
    }

    /**
     * @return The amount of the income.
     * @since 10
     */
    public double getAmount() {
        return this.amount;
    }

    /**
     * Sets the <code>amount</code> field.
     *
     * @param amount The amount of the income.
     * @since 10
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return a description of the income.
     * @since 1.0
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the <code>description</code> field.
     *
     * @param description a description of the income.
     * @since 1.0
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The name of the Income.
     * @since 1.0
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the <code>name</code> field.
     *
     * @param name The name of the Income.
     * @since 1.0
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The year of the transaction.
     * @since 1.0
     */
    public int getYear() {
        return this.year;
    }

    /**
     * Sets the <code>year</code> field.
     *
     * @param year The year of the transaction.
     * @since 1.0
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return The week of the month .
     * @since 1.0
     */
    public int getWeek() {
        return this.week;
    }

    /**
     * Sets the <code>week</code> field.
     *
     * @param week The week of the month .
     * @since 1.0
     */
    public void setWeek(int week) {
        this.week = week;
    }

    /**
     * @return get the mounth number between 0 and 52.
     * @since 1.0
     */
    public int getMonth() {
        return this.month;
    }

    /**
     * Sets the <code>month</code> field.
     *
     * @param Month get the month number between 0 and 52.
     * @since 1.0
     */
    public void setMonth(int Month) {
        this.month = Month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Income)) {
            return false;
        }
        Income income = (Income) o;
        return Double.compare(income.amount, amount) == 0 &&
               year == income.year &&
               week == income.week &&
               month == income.month &&
               moneyType == income.moneyType &&
               Objects.equals(description, income.description) &&
               Objects.equals(name, income.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(amount, moneyType, description, name, year, week, month);
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("amount", amount)
                .add("moneyType", moneyType)
                .add("description", description)
                .add("name", name)
                .add("year", year)
                .add("week", week)
                .add("month", month)
                .toString();
    }
}
