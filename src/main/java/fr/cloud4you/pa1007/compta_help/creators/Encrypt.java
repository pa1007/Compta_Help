package fr.cloud4you.pa1007.compta_help.creators;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class Encrypt {


    /**
     * Use this method to encrypt a name
     *
     * @param key     {@link Double}  month * amount
     * @param message {@link String} the name of the message to encrypt
     * @return {@link String} the encrypt data
     */
    public static String encryptMessage(Double key, String message) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(String.valueOf(key));
        return encryptor.encrypt(message);
    }

    /**
     * Use this method to decrypt a name
     *
     * @param key     {@link Double}  month * amount
     * @param message {@link String} the encrypted message to decrypt
     * @return {@link String} the decrypt data
     */
    public static String decryptMessage(Double key, String message) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(String.valueOf(key));
        return encryptor.decrypt(message);
    }
}
