package fr.cloud4you.pa1007.compta_help.creators;

public final class Commands {

    /**
     * Command to create an income .
     * 1-> Amount (decimal/double)<br>
     * 2-> Name (String)<br>
     * 3-> Description (String)<br>
     * 4-> Month (int)<br>
     * 5-> Week (int)<br>
     * 6-> Year (int)<br>
     * 7-> MoneyTypeNumber (0/1/2/50)<br>
     * 8-> Day (int)<br>
     * 9-> id (encryptedString)<br>
     */
    public static final String CREATE_INCOME =
            "INSERT INTO `Compta_help`.`Income` (`Amount`, `Name`, `Description`, `Month`, `Week`, `Year`, `Type`, `Day`, `ID`) VALUES (?,?,?,?,?,?,?,?,?);";

    /**
     * Command to create an income .
     * 1-> Price (decimal/double)<br>
     * 2-> Name (String)<br>
     * 3-> Description (String)<br>
     * 4-> Month (int)<br>
     * 5-> Week (int)<br>
     * 6-> Year (int)<br>
     * 7-> MoneyTypeNumber (0/1/2/50)<br>
     * 8-> Day (int)<br>
     * 9-> id (encryptedString)<br>
     * 10-> UsageType (See {@link fr.cloud4you.pa1007.compta_help.types.UsageType UsageType})
     */
    public static final String CREATE_COST =
            "INSERT INTO `Compta_help`.`Cost` (`Price`, `Name`, `Description`, `Month`, `Week`, `Year`, `Type`, `Day`, `ID`, `UsageType`) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?, ?);";

    /**
     * Command to replace an income .
     * 1-> Amount (decimal/double)<br>
     * 2-> Name (String)<br>
     * 3-> Description (String)<br>
     * 4-> Month (int)<br>
     * 5-> Week (int)<br>
     * 6-> Year (int)<br>
     * 7-> MoneyTypeNumber (0/1/2/50)<br>
     * 8-> Day (int)<br>
     * 9-> id (encryptedString) unchanged <br>
     */
    public static final String REPLACE_INCOME =
            "REPLACE INTO `Compta_help`.`Income` (`Amount`, `Name`, `Description`, `Month`, `Week`, `Year`, `Type`, `Day`, `ID`) VALUES (?,?,?,?,?,?,?,?,?);";

    /**
     * Command to replace an income .
     * 1-> Price (decimal/double)<br>
     * 2-> Name (String)<br>
     * 3-> Description (String)<br>
     * 4-> Month (int)<br>
     * 5-> Week (int)<br>
     * 6-> Year (int)<br>
     * 7-> MoneyTypeNumber (0/1/2/50)<br>
     * 8-> Day (int)<br>
     * 9-> id (encryptedString) unchanged <br>
     * 10-> UsageType (See {@link fr.cloud4you.pa1007.compta_help.types.UsageType UsageType})
     */
    public static final String REPLACE_COST =
            "REPLACE INTO `Compta_help`.`Cost` (`Price`, `Name`, `Description`, `Month`, `Week`, `Year`, `Type`, `Day`, `ID`, `UsageType`) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?, ?);";


    /**
     * Command to replace an income .
     * 1-> Amount (decimal/double)<br>
     * 2-> Name (String)<br>
     * 3-> ownerName (String)<br>
     * 4-> creationDate (Date)
     */
    public static final String CREATE_ACCOUNT =
            "INSERT INTO `Compta_help`.`Balance` (` Money`, `Name`, `Type`, `OwnerName`, `creationDate`) VALUES (?, ?, 'Bank', ?,?);";

    /**
     * get the income database
     */
    public static final String DATA_BASE_INCOME = "SELECT * FROM `Income`";

    /**
     * get the cost database
     */
    public static final String DATA_BASE_COST = "SELECT * FROM `Cost`";

    /**
     * get the bank account database
     */
    public static final String DATA_BASE_ACCOUNT = "SELECT * FROM `Balance` WHERE Type = 'Bank' ";

    /**
     * get the bank account database
     */
    public static final String DATA_BASE_CASH = "SELECT * FROM `Balance` WHERE Type = 'Cash' ";

    /**
     * To replace the money with the name of the account
     * 1-> new Money (double)<br>
     * 2->AccountName (String)
     */
    public static final String REPLACE_MONEY_ACCOUNT = "UPDATE Balance SET  ` Money` = ? WHERE Name = ? ;";

    /**
     * To replace the cash in the database <br>
     * 1->new Money (double) <br>
     */
    public static final String REPLACE_MONEY_CASH = "UPDATE Balance SET  ` Money` = ? WHERE Type = 'Cash' ;";
}
