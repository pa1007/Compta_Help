package fr.cloud4you.pa1007.compta_help.creators;

import java.util.Objects;

public class Cash {


    /**
     * The amount of money you have in cash.
     *
     * @since 1.0
     */
    private double amount;


    /**
     * The last transaction done
     *
     * @since 1.0
     */
    private Transaction lastTransaction;

    public Cash(double amount) {
        this.amount = amount;
    }

    /**
     * @return The last transaction done
     * @since 1.0
     */
    public Transaction getLastTransaction() {
        return this.lastTransaction;
    }

    /**
     * Sets the <code>lastTransaction</code> field.
     *
     * @param lastTransaction The last transaction done
     * @since 1.0
     */
    public void setLastTransaction(Transaction lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    /**
     * @return The amount of money you have in cash.
     * @since 1.0
     */
    public double getAmount() {
        return this.amount;
    }

    /**
     * Sets the <code>amount</code> field.
     *
     * @param amount The amount of money you have in cash.
     * @since 1.0
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cash)) {
            return false;
        }
        Cash cash = (Cash) o;
        return Double.compare(cash.amount, amount) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(amount);
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("amount", amount)
                .toString();
    }
}
