package fr.cloud4you.pa1007.compta_help.creators;

import java.util.Date;

public class Transaction {

    /**
     * The date of the transaction.
     *
     * @since 1.0
     */
    private Date transactionDate;

    /**
     * The name of the account.
     *
     * @since 1.0
     */
    private String accountName;

    /**
     * The amount of cash before the transaction.
     *
     * @since 1.0
     */
    private double cashBefore;

    /**
     * The amount of the transaction.
     *
     * @since 1.0
     */
    private double amount;

    /**
     * The money in the account before the transaction.
     *
     * @since 1.0
     */
    private double accountMoneyBefore;


    /**
     * if the path of the transaction is from the cash to the account.
     *
     * @since 1.0
     */
    private boolean cashToAccount;

    public Transaction(
            String accountName,
            double cashBefore,
            double amount,
            Date transactionDate,
            double accountMoneyBefore,
            boolean cashToAccount
    ) {
        this.accountName = accountName;
        this.cashBefore = cashBefore;
        this.amount = amount;
        this.transactionDate = transactionDate;
        this.accountMoneyBefore = accountMoneyBefore;
        this.cashToAccount = cashToAccount;
    }

    /**
     * @return if the path is from the cash to the account.
     * @since 1.0
     */
    public boolean getCashToAccount() {
        return this.cashToAccount;
    }

    /**
     * Sets the <code>cashToAccount</code> field.
     *
     * @param cashToAccount if the path is from the cash to the account.
     * @since 1.0
     */
    public void setCashToAccount(boolean cashToAccount) {
        this.cashToAccount = cashToAccount;
    }

    /**
     * @return The date of the transaction.
     * @since 1.0
     */
    public Date getTransactionDate() {
        return this.transactionDate;
    }

    /**
     * Sets the <code>transactionDate</code> field.
     *
     * @param transactionDate The date of the transaction.
     * @since 1.0
     */
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return The amount of the transaction.
     * @since 1.0
     */
    public double getAmount() {
        return this.amount;
    }

    /**
     * Sets the <code>amount</code> field.
     *
     * @param amount The amount of the transaction.
     * @since 1.0
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return The money in the account before the transaction.
     * @since 1.0
     */
    public double getAccountMoneyBefore() {
        return this.accountMoneyBefore;
    }

    /**
     * Sets the <code>accountMoneyBefore</code> field.
     *
     * @param accountMoneyBefore The money in the account before the transaction.
     * @since 1.0
     */
    public void setAccountMoneyBefore(double accountMoneyBefore) {
        this.accountMoneyBefore = accountMoneyBefore;
    }

    /**
     * @return The amount of cash before the transaction.
     * @since 1.0
     */
    public double getCashBefore() {
        return this.cashBefore;
    }

    /**
     * Sets the <code>cashBefore</code> field.
     *
     * @param cashBefore The amount of cash before the transaction.
     * @since 1.0
     */
    public void setCashBefore(double cashBefore) {
        this.cashBefore = cashBefore;
    }

    /**
     * @return The name of the account.
     * @since 1.0
     */
    public String getAccountName() {
        return this.accountName;
    }

    /**
     * Sets the <code>accountName</code> field.
     *
     * @param accountName The name of the account.
     * @since 1.0
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
