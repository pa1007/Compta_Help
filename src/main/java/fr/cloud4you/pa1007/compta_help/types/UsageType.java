package fr.cloud4you.pa1007.compta_help.types;

public enum UsageType {

    ENTERTAINING,
    COMPUTER,
    HOUSE,
    VIDEO_GAMES,
    FOOD,
    TRANSFER,
    CAR,
    GAS,
    USELESS_THINGS,
    REPAIR,
    PRESENT,
    SUPERMARKET,
    TAXES,
    RENTAL,
    MOBILE,
    ONLINE,
    TWITCH_SUB,
    OTHER
}