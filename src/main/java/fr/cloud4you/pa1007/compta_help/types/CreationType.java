package fr.cloud4you.pa1007.compta_help.types;

public enum CreationType {
    COST,
    INCOME
}
