package fr.cloud4you.pa1007.compta_help.types;

/**
 * The type of money use to do transaction
 */
public enum MoneyType {

    BANK(0),
    CASH(1),
    FREE(2),
    OTHER(50);

    MoneyType(final int number) {
    }

    public static int getValue(MoneyType type) {
        switch (type) {
            case BANK:
                return 0;
            case CASH:
                return 1;
            case FREE:
                return 2;
            case OTHER:
                return 50;
            default:
                return Integer.MIN_VALUE;
        }
    }

    public static MoneyType valueOf(int type) {
        switch (type) {
            case 0:
                return BANK;
            case 1:
                return CASH;
            case 50:
                return OTHER;
            case 2:
                return FREE;
            default:
                return null;
        }
    }

    public static MoneyType getType(String name) {
        switch (name) {
            case "BANK":
                return BANK;
            case "CASH":
                return CASH;
            case "OTHER":
                return OTHER;
            case "FREE":
                return FREE;
            default:
                return null;
        }

    }
}
