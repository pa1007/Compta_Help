package fr.cloud4you.pa1007.compta_help.constants;


import javafx.scene.paint.Color;

public final class Constants {

    public static final String URL = "jdbc:mysql:";

    public static final String USER_NAME = "";

    public static final String USER_PASSWORD = "";

    /**
     * get the income database
     */
    public static final String DATA_BASE_INCOME = "SELECT * FROM `Income`";

    /**
     * get the cost database
     */
    public static final String DATA_BASE_COST = "SELECT * FROM `Cost`";

    /**
     * get the account database
     */
    public static final String DATA_BASE_ACCOUNT = "SELECT * FROM `Balance`";

    /**
     * Color use to fill the indicator
     */
    public static final Color GRAY_FILL = Color.color(199, 199, 199);
}
